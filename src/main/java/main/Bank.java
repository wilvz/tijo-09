package main;

import java.util.ArrayList;

public class Bank implements BankOperation {



    ArrayList<Account> accounts = new ArrayList();


    public int createAccount() {
        int accountNumber=accounts.isEmpty()?1:accounts.get(accounts.size()-1).getAccountNumber()+1;
        Account account = new Account(accountNumber,0);
        accounts.add(account);

        return account.getAccountNumber();

    }

    public int deleteAccount(int accountNumber) {

        for(Account a : accounts){
            if(a.getAccountNumber()==accountNumber){
                int deleted= accounts.indexOf(a);
                accounts.remove(a);
                return deleted-1;
            }
        }
            return ACCOUNT_NOT_EXISTS;
    }

    public boolean deposit(int accountNumber, int amount) {
        if(amount<=0)
            return false;
        for(Account a : accounts){
            if(a.getAccountNumber()==accountNumber ){

                    a.setAccountBalance(a.getAccountBalance()+amount);

                    return true;


            }
        }
        return false;
    }

    public boolean withdraw(int accountNumber, int amount) {
        if(amount<=0)
            return false;

        for(Account a : accounts){
            if(a.getAccountNumber()==accountNumber && amount<a.getAccountBalance()){
                a.setAccountBalance(a.getAccountBalance()-amount);

                return true;
            }
        }
        return false;
    }

    public boolean transfer(int fromAccount, int toAccount, int amount) {

        if(amount<=0)
            return false;

        Account sender = null;
        Account reciever =null;
        for(Account a : accounts){
            if(a.getAccountNumber()==fromAccount){
                sender = a;
            }else if( a.getAccountNumber() == toAccount){
                reciever = a;
            }
        }

        if(sender != null && reciever != null && sender.getAccountBalance()>amount){
            sender.setAccountBalance(sender.getAccountBalance()-amount);
            reciever.setAccountBalance(reciever.getAccountBalance()+amount);
            return true;
        }

        return false;
    }

    public int accountBalance(int accountNumber) {

        for(Account a : accounts){
            if(a.getAccountNumber()==accountNumber){
                return a.getAccountBalance();
            }
        }
        return ACCOUNT_NOT_EXISTS;
    }



    public int sumAccountsBalance() {
        int sum=0;
        for(Account a : accounts){
            sum+=a.getAccountBalance();
        }

        return sum;
    }
}