package main;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class AccountTest {
    private BankOperation account;
    @BeforeEach
    public void initDataForAccount() {
        account = new Bank();
    }
    @Test
    public void checkCorrectOperationForAccount() {
        /* test create account */
        assertEquals(1, account.createAccount(), "Zalozenie konta o numerze: 1");
        assertEquals(2, account.createAccount(), "Zalozenie konta o numerze: 2");
        assertEquals(3, account.createAccount(), "Zalozenie konta o numerze: 3");
        assertEquals(4, account.createAccount(), "Zalozenie konta o numerze: 4");
        /* test delete account */
        assertEquals(0, account.deleteAccount(2), "Likwidacja konta nr. 2");
        assertEquals(-1, account.deleteAccount(5 ), "Likwidacja konta nr. 5");
        /* test create account */
        assertEquals(5, account.createAccount(), "Zalozenie konta o numerze: 5");
        /* test deposit amount */
        assertTrue(account.deposit( 1, 100), "Wplacam 100 zl na nr. 1");

        assertFalse(account.deposit( 2, 200), "Wplacam 200 zl na nr. 2");
        assertTrue(account.deposit( 3, 300), "Wplacam 300 zl na nr. 3");
        assertTrue(account.deposit( 4, 400), "Wplacam 400 zl na nr. 4");
        assertTrue(account.deposit( 5, 500), "Wplacam 500 zl na nr. 5");
        assertTrue(account.deposit( 5, 450), "Wplacam 450 zl na nr. 5");
        assertFalse(account.deposit( 4, -5), "Wplacam -5 zl na nr. 1");
        /* test account balance */
        assertEquals(100, account.accountBalance(1), "Stan konta nr. 1");
        assertEquals(300, account.accountBalance(3), "Stan konta nr. 3");
        assertEquals(950, account.accountBalance(5), "Stan konta nr. 5");
        /* test withdraw amount from account */
        assertTrue(account.withdraw( 1, 50), "Wyplacam 50 zl z nr. 1");
        assertTrue(account.withdraw( 3, 75), "Wyplacam 75 zl z nr. 3");
        assertFalse(account.withdraw( 5, 1000), "Wyplacam 1000 zl z nr. 5");
        /* test account balance */
        assertEquals(50, account.accountBalance(1), "Stan konta nr. 1");
        assertEquals(225, account.accountBalance(3), "Stan konta nr. 3");
        assertEquals(950, account.accountBalance(5), "Stan konta nr. 5");
        /* test account transfer - 1 - */
        assertTrue(account.transfer(3, 5, 25),
                "Przelew z konta nr. 3 na konto nr. 5 (25 zl)");
        /* test transaction result */
        assertEquals(200, account.accountBalance(3), "Stan konta nr. 3");
        assertEquals(975, account.accountBalance(5), "Stan konta nr. 5");
        /* test account transfer - 2 - */
        assertFalse(account.transfer(3, 5, 210),
                "Przelew z konta nr. 3 na konto nr. 5 (210 zl)");
        /* test transaction result */
        assertEquals(200, account.accountBalance(3), "Stan konta nr. 3");
        assertEquals(975, account.accountBalance(5), "Stan konta nr. 5");
        /* test account transfer - 3 - */
        assertFalse(account.transfer(3, -1, 200),
                "Przelew z konta nr. 3 na konto nr. 5 (200 zl)");
        /* test transaction result */
        assertEquals(200, account.accountBalance(3), "Stan konta nr. 3");
        assertEquals(975, account.accountBalance(5), "Stan konta nr. 5");
        /* test account balance */
        assertEquals(50, account.accountBalance(1), "Stan konta nr. 1");
        assertEquals(-1, account.accountBalance(2), "Stan konta nr. 2");
        assertEquals(200, account.accountBalance(3), "Stan konta nr. 3");
        assertEquals(400, account.accountBalance(4), "Stan konta nr. 4");
        assertEquals(975, account.accountBalance(5), "Stan konta nr. 5");
        assertEquals(-1, account.accountBalance(6), "Stan konta nr. 6");
        /* test create account */
        assertEquals(6, account.createAccount(), "Zalozenie konta o numerze: 6");
        /* test account balance */
        assertEquals(0, account.accountBalance(6), "Stan konta nr. 6");
        /* test sum account balance */
        assertEquals(1625, account.sumAccountsBalance(), "Stan skarbca");
    }
}
